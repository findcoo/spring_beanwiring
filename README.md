##spring bean 와이어링 기본선언

1. *디렉토리 구성*
```
./src/
|-- main
|   |-- java
|   |   `-- springidol
|   |       |-- Juggler.java
|   |       |-- Main.java
|   |       |-- PerformanceException.java
|   |       `-- Performer.java
|   `-- resources
|       `-- spring-idol.xml
```
---
2. *주의 및 참고*
    + `<bean id="duke" class="springidol.Juggler"/>` 와 같이 빈 선언
    + build.gradle 설정 참조 (메이븐 저장소 참조)
    + Main.java 에서 빈 호출
    + resources 디렉토리안에 bean 설정 파일을 둘 것
---
3. *빈 네임스페이스*

| 네임스페이스 | 목적         |
| :--------- |-----------: |
| `aop`        | 엑스펙트 선언을 위한 엘리먼트와 @AspectJ 애너테이션이 적용된 클래스를 자동으로 스프링 애스펙트로 프록시하는 엘리먼트 제공 |
| `beans`      | 빈의 선언과 연결 방법을 정의 할 수 있도록 한다 |
| `context`    | 스프링에 관리되지않는 객체의 주입과 빈을 오토디텍트하고 오토와이어링 기능을 포함하도록 애플리케이션 컨텍스트를 설정한다 |
| `jee`        | JNDI 와 EJB등의 JAVA EE 통합 환경 제공 |
| `jms`        | 메시지 드리븐 POJO를 선언하기 위한 설정 엘리먼트를 제공한다 |
| `lang`       | Grooby, JRuby, 배쉬쉘 스크립트로 구현되는 빈의 선언을 가능하게 한다 |
| `mvc`        | 애너테이션 지향 컨트롤러, 뷰 컨트롤러, 인터셉터 등의 MVC 기능을 가능하게 한다 |
| `oxm`        | 스프링의 객체 대 XML 매핑 구조의 설정을 지원한다 |
| `tx`        | 선언적 트랜잭션 설정을 제공한다 |
| `util`       | 컬렉션을 빈으로 선언하는 기능, 프로퍼티 대치 엘리먼트에 대한 지원을 포함한다 |

    
